class Deal():
    headline = ''
    description = ''
    shop_name = ''
    shop_address = ''
    address = ''
    mall_name = ''
    start_time = 0
    duration = 0

    def __init__(self, start, end):
        self.start_time = start
        self.duration = end

    def serialize(self):
        return {
            "headline": self.headline,
            "description": self.description,
            "shop": self.shop_name,
            "address": self.address,
            "start_time": self.start_time,
            "duration": self.duration,
            "mall": self.mall_name,
            "shop_address": self.shop_address
        }


class Utility():
    floor = 0
    name = ''
    mall_name = ''

    def __init__(self, floor, name, mall):
        self.floor = floor
        self.name = name
        self.mall_name = mall

    def serialize(self):
        return {
            "floor": self.floor,
            "name": self.name
        }


class Floor():
    number = 0
    shops = []

    def __init__(self, number, shops):
        self.number = number
        self.shops = shops

    def serialize(self):
        return{
            "number": self.number,
            "shops": self.shops
        }


class Map():
    food = []
    utilities = []
    floors = []

    def get_utilities(self):
        for i in range(len(self.floors)):
            u = Utility(self.floors[i].number, 'Restroom', '')
            self.utilities.append(u)
        print("after setting utils. Size is [ " + str(len(self.utilities)))
        return self.utilities


class NotificationModel(object):
    notification = None
    to = ""

    def __init__(self, title, body):
        self.notification = Notification(title, body)


class Notification(object):
    title = ''
    body = ''

    def __init__(self, title, body):
        self.title = title
        self.body = body
