import yaml
from models import Deal, Utility, Floor


class DataHolder():
    all_deals = []

    def __init__(self, file_name):
        stream = open(file_name, 'r')
        result = yaml.load_all(stream)
        print(result)
        for dList in result:
            for i in range(len(dList)):
                data = dList[i]
                deal = Deal(data["start"], data["duration"])
                deal.headline = data['headline']
                deal.description = data['description']
                deal.mall_name = data['mall']
                deal.shop_name = data['shop']
                deal.shop_address = data['shop_address']
                self.all_deals.append(deal)


def get_for_mall(data_holder, mall_name):
    filtered = []
    for i in range(len(data_holder.all_deals)):
        if data_holder.all_deals[i].mall_name == mall_name:
            filtered.append(data_holder.all_deals[i])
    return filtered


def get_food_items(mall):
    items = []
    stream = open('food.yml', 'r')
    result = yaml.load_all(stream)
    for d in result:
        for i in range(len(d)):
            data = d[i]
            if data['mall'] == mall:
                utils = Utility(data['floor'], data['name'], data['mall'])
                items.append(utils)
    return items


def get_floors(mall):
    items = []
    stream = open('floors.yml', 'r')
    result = yaml.load_all(stream)
    for d in result:
        for i in range(len(d)):
            data = d[i]
            if data['mall'] == mall:
                shops = data['shops'].split(',')
                items.append(Floor(data['number'], shops))
    return items
