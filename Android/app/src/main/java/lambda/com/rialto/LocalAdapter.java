package lambda.com.rialto;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kunall17 on 6/11/16.
 */

public class LocalAdapter extends RecyclerView.Adapter<LocalAdapter.LocalAdapterViewHolder> {
    List<Deal> dealList;
    Context context;
    private static String TAG = LocalAdapter.class.getName();

    public LocalAdapter(List<Deal> dealList, Context context) {
        this.dealList = dealList;
        this.context = context;
    }

    @Override
    public LocalAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.localdeal_item, parent, false);
        return new LocalAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final LocalAdapterViewHolder holder, int position) {
        Deal deal = dealList.get(position);
        holder.timestampTV.setText("Posted on - " + deal.start_time.toString().replace("_", ""));
        holder.descTV.setText(deal.description);
        holder.headingTV.setText(deal.headline);
        holder.locationTV.setText(deal.shop_address);
    }

    @Override
    public int getItemCount() {
        return dealList.size();
    }


    class LocalAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.headingTV)
        TextView headingTV;
        @BindView(R.id.timestampTV)
        TextView timestampTV;
        @BindView(R.id.descTV)
        TextView descTV;
        @BindView(R.id.locationTV)
        TextView locationTV;

        LocalAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
