package lambda.com.rialto;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static RetrofirInterface retrofitClientService;

    public static RetrofirInterface getClient() {
        if (retrofitClientService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://46.101.37.183:8081")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            retrofitClientService = retrofit.create(RetrofirInterface.class);
        }
        return retrofitClientService;
    }
}
