package lambda.com.rialto;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ExploreAdapter extends BaseExpandableListAdapter {
    private static final String TAG = "ASD";
    Explore explore;
    Context context;


    public ExploreAdapter(Explore explore, Context context) {
        this.explore = explore;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return 2 + explore.floors.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        switch (groupPosition) {
            case 0:
                return explore.food.size();
            case 1:
                return explore.utilities.size();
            default:
                explore.floors.get(groupPosition - 2).shops.size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        switch (groupPosition) {
            case 0:
                return explore.food;
            case 1:
                return explore.utilities;
            case 2:
                return explore.floors.get(groupPosition - 2).shops;
        }
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        switch (groupPosition) {
            case 0:
                return explore.food.get(childPosition);
            case 1:
                return explore.utilities.get(childPosition);
            default:
                return explore.floors.get(groupPosition - 2).shops.get(childPosition);
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = null;// (String) getGroup(groupPosition);
        switch (groupPosition) {
            case 0:
                headerTitle = "Food";
                break;
            case 1:
                headerTitle = "Utility";
                break;
            case 2:
                headerTitle = "Ground Floor";
                break;
            default:
                headerTitle = "Floor " + (groupPosition - 2);
                break;
        }
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String childText = "";

        switch (groupPosition) {
            case 0:
                childText = explore.food.get(childPosition).name;
                break;
            case 1:
                childText = explore.utilities.get(childPosition).name;
                break;
            default:
                childText = explore.floors.get(groupPosition - 2).shops.get(childPosition);
        }


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}