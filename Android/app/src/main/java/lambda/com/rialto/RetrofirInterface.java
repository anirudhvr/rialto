package lambda.com.rialto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

interface RetrofirInterface {
    @GET("/api/deals")
    Call<Deal[]> getDeals();

    @GET("/api/Mall%20of%20India/deals/")
    Call<List<Deal>> getMallDeals();
    @GET("/api/Mall%20of%20India/map/")
    Call<Explore> getExplore();
}
