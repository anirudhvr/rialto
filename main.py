from flask import Flask, jsonify
from flask import render_template, request, session
import dataservice
from models import Map, Deal, NotificationModel
import jsonpickle
import threading
import requests

app = Flask(__name__)
app.debug = True
app.secret_key = "something very secret should be set in production"

data = None
search_terms = ""


@app.route("/api/deals")
def get_all_deals():
    all_deals = data.all_deals
    return jsonpickle.encode(all_deals, unpicklable=False)


@app.route("/api/<mall>/map/")
def get_map(mall):
    map = Map()
    map.food = dataservice.get_food_items(mall)
    map.floors = dataservice.get_floors(mall)
    map.utilities = []
    map.utilities = map.get_utilities()
    return jsonpickle.encode(map, unpicklable=False)


@app.route("/api/<mall>/deals/")
def get_filtered_deals(mall):
    all_deals = dataservice.get_for_mall(data, mall)
    return jsonpickle.encode(all_deals, unpicklable=False)


@app.route("/search/<term>")
def search(term):
    session['search'] = term
    return jsonify({"message": "okay"})


@app.route("/dashboard", methods=['GET', 'POST'])
def dashboard():
    if request.method == 'POST':
        headline = request.form['headline']
        description = request.form['description']
        valid_for = request.form['validity']
        send_notification(headline, description, valid_for)
        session['search'] = None
        term = None
        error = 'Success'
    else:
        error = None
        term = session['search']
    return render_template("dash.html", pending=term, error=error)


def send_notification(headline, description, valid_for):
    deal = Deal("12/06/2016", valid_for)
    deal.description = description
    deal.headline = headline
    deal.shop_name = "Cotton House"
    deal.shop_address = "Shop No. 12, First Floor"
    deal.mall_name = "Mall of India"
    data.all_deals.append(deal)
    json = NotificationModel(headline, description)
    # json = "{\"notification\":{\"body\":\"" + description + \
    #     "\", \"title\":\"" + headline + "\"},\"to\":\"\"}"
    post_notification(jsonpickle.encode(json, unpicklable=False))


def post_notification(json):
    headers = {'Authorization':'key=AIzaSyB8oGu1mSdCDVa7HF2u96C57a8Sz3AB_3E'}
    r = requests.post("https://fcm.googleapis.com/fcm/send",
                      data=json, headers=headers)
    print("FCM Status " + str(r.status_code))


if __name__ == "__main__":
    data = dataservice.DataHolder("data.yml")
    app.run()
    app.debug = True
